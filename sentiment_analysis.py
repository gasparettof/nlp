def sentiment_analysis(text):
    """
    Perform sentiment analysis on the input text using the sentiment-analysis model.

    Args:
    - text (str): The input text for sentiment analysis.

    Returns:
    - result: The sentiment analysis result for the input text.
    """
    from transformers import pipeline

    sentiment_analysis = pipeline("sentiment-analysis", model="nlptown/bert-base-multilingual-uncased-sentiment")

    result = sentiment_analysis(text)
    return result

# Usage
text = input("Enter the text for sentiment analysis: ")
result = sentiment_analysis(text)
print(result)

