def auto_complete_text(prompt):
    """
    Provide auto-completion for the English text prompt using the T5-Base model.

    Args:
    - prompt (str): The English text prompt for auto-completion.

    Returns:
    - result: The auto-completed text.
    """
    from transformers import pipeline

    text_generator = pipeline("text2text-generation", model="t5-base")

    result = text_generator(prompt, max_length=150, min_length=50, do_sample=True, temperature=0.8)
    return result[0]['generated_text']

# Request user input for the English prompt
prompt = input("Enter the text prompt for auto-completion (in English): ")

# Get the auto-completed text
result = auto_complete_text(prompt)
print(result)

