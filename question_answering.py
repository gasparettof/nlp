def question_answering():
    """
    Perform question answering based on the given context and question using the Robustly Optimized BERT Approach (RoBERTa) model.

    Returns:
    - result: The answer to the question.
    """
    from transformers import pipeline

    question_answering = pipeline("question-answering", model="deepset/roberta-base-squad2", tokenizer="deepset/roberta-base-squad2")

    context = input("Enter the context: ")
    question = input("Enter the question: ")

    result = question_answering(question=question, context=context)
    return result

# Get the answer to the question from the user-provided context and question using RoBERTa
result = question_answering()
print(result)

