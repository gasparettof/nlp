def language_translation(text):
    """
    Translate text from Italian to English using a dedicated Italian translation model.

    Args:
    - text (str): The text to translate from Italian to English.

    Returns:
    - result: The translated text.
    """
    from transformers import pipeline

    translator = pipeline("translation", model="Helsinki-NLP/opus-mt-it-en")

    result = translator(text, source_language="it", target_language="en")
    return result[0]['translation_text']

# Usage
text = "Ciao, come stai?"
result = language_translation(text)
print(result)

