# Transformer Libraries Demo Scripts

This repository contains a set of Python scripts that demonstrate the usage of transformer libraries for natural language processing tasks. Each script showcases a different function utilizing transformer models. Below are the scripts and their functionalities:

## Scripts:

### 1. `autocomplete.py`
- **Description:** This script demonstrates the autocomplete feature using transformer models. It prompts the user for input and shows autocomplete suggestions based on the input text.

### 2. `question_answering.py`
- **Description:** This script showcases question-answering functionality with transformer models. It allows users to input a context and a question, and returns the answer based on the context provided.

### 3. `sentiment_analysis.py`
- **Description:** The sentiment analysis script uses transformer models to analyze the sentiment of user-input text. Users are prompted to enter text, and the script provides sentiment analysis results.

### 4. `summarization.py`
- **Description:** This script highlights text summarization using transformer models. It takes user input, summarizes the text, and displays the summarized content.

### 5. `translation.py`
- **Description:** The translation script demonstrates language translation capabilities with transformer models. Users enter text in one language, and the script translates it into another language.

