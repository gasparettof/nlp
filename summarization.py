def text_summarization(text):
    """
    Summarize the input text using an English model for text summarization.

    Args:
    - text (str): The input text to summarize.

    Returns:
    - result: The summarized text.
    """
    from transformers import pipeline

    summarizer = pipeline("summarization", model="facebook/bart-large-cnn")

    result = summarizer(text, max_length=100, min_length=30, do_sample=False)
    return result[0]['summary_text']

# Request user input
text = input("Insert the text you want to summarize: ")

# Get the text summary
result = text_summarization(text)
print(result)

